<?php
return [
    'user_1' => [
        'id' => '1',
        'username' => 'user',
        'auth_key' => 'yXk-0rqi-MB_Y2-jxVEAfCqbZcnePR1a',
        'password_hash' => '$2y$13$12y5EVVgnNtcyuDh3f54rOmzuudaLUcJu02w.5bob5.nNUDJWzKi2',
        'email' => 'sergey.sirotenko.199@gmail.com',
        'status' => '10',
        'created_at' => '1588875539',
        'updated_at' => '1589112576'
    ],
];