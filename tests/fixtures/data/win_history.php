<?php
return [
    'wh_1' => [
        'id' => '1',
        'user_id' => '1',
        'type' => 'cash',
        'value' => '100',
        'promo' => 'NULL',
        'created_at' => '1589046589',
    ],
    'wh_2' => [
        'id' => '2',
        'user_id' => '1',
        'type' => 'bonus',
        'value' => '1000',
        'promo' => 'NULL',
        'created_at' => '1589046590',
    ],
    'wh_3' => [
        'id' => '3',
        'user_id' => '1',
        'type' => 'object',
        'value' => '0',
        'promo' => '/img/prize_icons/2.jpg',
        'created_at' => '1589046591',
    ]
];