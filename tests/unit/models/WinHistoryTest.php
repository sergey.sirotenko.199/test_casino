<?php namespace models;

;
use app\tests\fixtures\UserFixture;
use app\tests\fixtures\WinHistoryFixture;
use app\models\WinHistory;

class WinHistoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    public function _fixtures() {
        return [
            'users' => UserFixture::className(),
            'win_history' => WinHistoryFixture::className() 
        ];
    }
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testTryConvertCashTransaction()
    {
        WinHistory::convertCashTransaction(1);
        $win_history = $this->tester->grabFixture('win_history', 'wh_1');

        expect($win_history->value)->equals(1500);
        expect($win_history->type)->equals('bonus');
    }  
    
    public function testTryConvertObjectTransaction()
    {
        WinHistory::convertCashTransaction(3);
        $win_history = $this->tester->grabFixture('win_history', 'wh_3');

        expect($win_history->value)->equals(0);
        expect($win_history->type)->equals('object');
    }
}