<?php

namespace app\controllers;

use Yii;;
use yii\web\Controller;
use app\models\CasinoPage;
use app\models\WinHistory;
use app\models\User;
use yii\filters\AccessControl;

class CasinoController extends Controller {
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $casinoPage = new CasinoPage();

        return $this->render('index', [
            'user'          => $casinoPage['user'],
            'win_history'   => $casinoPage['win_history'],
            'errors'        => $casinoPage['errors']
        ]);
        return $this->render('index');
    }

    public function actionBegin() {
        $casinoPage = new CasinoPage();
        $casinoPage->renderPrize();

        return $this->render('index', [
            'user'                  => $casinoPage['user'],
            'prize'                 => $casinoPage['prize'],
            'win_history'           => $casinoPage['win_history'],
            'last_transaction_id'   => $casinoPage['last_transaction_id'],
            'errors'                => $casinoPage['errors']
        ]);
    }
    public function actionConvert() {
        $request = Yii::$app->request;
        return $this->render('convert', [
            'conv_result' => WinHistory::convertCashTransaction($request->get('t_id'))
        ]);
    }

    public function actionRefuse() {
        $request = Yii::$app->request;
        return $this->render('refuse', [
            'refuse_result' => WinHistory::refuseOnPrize($request->get('t_id'))
        ]);
    }

    public function actionSend() {
        $request = Yii::$app->request;
        return $this->render('send', [
            'status' => WinHistory::sendPrizeOnEmail($request->get('t_id')),
        ]);
    }

    public function actionCashOut() {
        $request = Yii::$app->request;
        return $this->render('cash_out', [
            'status' => WinHistory::cashOut($request->get('t_id'))
        ]);
    }
}