<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/** 
 * UserPrize model
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property integer $value
 * @property string $promo
 * @property integer $created_at
 * 
*/

class WinHistory extends ActiveRecord {
    /**
     * @inheritdoc
     */

    const COEFFICENT = 15;

    public $user;

    public static function tableName()
    {
        return '{{%win_history}}';
    }

    public static function convertCashTransaction($transaction_id) {
        $transaction = self::findOne(['id' => $transaction_id]);
        $user = User::findOne(['id' => Yii::$app->user->id]);

        if(!isset($transaction) || $transaction->type != 'cash') return 0;

        $transaction->type = 'bonus';
        $transaction->value = $transaction->value * self::COEFFICENT;
        $transaction->save();

        return  $transaction->value;
    }
    
    public static function refuseOnPrize($transaction_id) {
        $transaction = self::findOne(['id' => $transaction_id]);
        $user = User::findOne(['id' => Yii::$app->user->id]);

        if(!isset($transaction) || $transaction->type != 'object') return 0;

        self::deleteAll(['id' => $transaction->id]);
        return $transaction_id;
    }

    public static function sendPrizeOnEmail($transaction_id) {
        $transaction = self::findOne(['id' => $transaction_id]);
        $user = User::findOne(['id' => Yii::$app->user->id]);

        if(!isset($transaction) || $transaction->type != 'object') return 0;
        var_dump($user->email);

        $send_result = Yii::$app->mailer->compose()
            ->setFrom('no-reply@example.com')
            ->setTo($user->email)
            ->setSubject('Your prize!')
            ->setTextBody('Your prize!')
            ->setHtmlBody('<img src="casino.loc/'.$transaction->promo.'" />')
            ->send();

        if(!$send_result) return 0;
        
        self::refuseOnPrize($transaction->id);
        return 1;
    }

    public static function cashOut($transaction_id) {
        $transaction = self::findOne(['id' => $transaction_id]);
        $user = User::findOne(['id' => Yii::$app->user->id]);

        if(!isset($transaction) || $transaction->type != 'cash') return 0;
        //There should be an API call here
        //##############
        //##############
        //##############
        //##############

        //simulate a crash in the API 3%
        $r = round(1,100);
        if($r > 97) {
            return 0;
        }

        $user->cash_val =  $user->cash_val - $transaction->value;
        $user->save();
        self::deleteAll(['id' => $transaction->id]);

        return 1 ;
    }

    public static function getUserCash() {
        $user = User::findOne(['id' => Yii::$app->user->id]);
        return self::find()->where([ 'user_id' => $user->id, 'type' => 'cash' ])->sum('value');
    }

    public static function getUserBonuses() {
        $user = User::findOne(['id' => Yii::$app->user->id]);
        return self::find()->where([ 'user_id' => $user->id, 'type' => 'bonus' ])->sum('value');
    }

    public static function getBonusesFromTransaction($value) {
        $user = User::findOne(['id' => Yii::$app->user->id]);

        while($value > 0) {
            $used_transactions = self::find()
            ->where([ 'user_id' => $user->id, 'type' => 'bonus'])
            ->andWhere(['not', ['value' => 0]])
            ->one();

            $difference = $used_transactions->value - $value;

            if($difference > 0) {
                $used_transactions->value = $used_transactions->value - $value;
                $value = 0;
            }else{
                $value = $value - $used_transactions->value;
                $used_transactions->value = 0;
            }
            
            $used_transactions->save();   
        }
    }
}

?>