<?php
namespace app\models\data;

use app\models\data\Prize;

class PrizeObject extends Prize {
    public function __construct($type = null, $cash_val = 0) {
        parent::__construct($type, $cash_val);
        $this->img_url = $this->img_stack[rand(0,count($this->img_stack) - 1)];
    }
    private $img_stack = [
        '/img/prize_icons/1.png',
        '/img/prize_icons/2.jpg',  
        '/img/prize_icons/3.jpg'
    ];
}
?>