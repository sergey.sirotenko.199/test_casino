<?php
namespace app\models\data;

class Prize {
    function __construct($type = null, $win_val = 0) {
        $this->win_val = $win_val;
        $this->type = $type;
        switch($type) {
            case('cash'):
                $this->win_title = 'You have a cash reward: '.$win_val.'$';
                break;
            case('bonus'):
                $this->win_title = 'You have a bonus reward: '.$win_val;
                break;
            case('object'):
                $this->win_title = 'You have a prize:';
                break;
        }
    }
}
?>