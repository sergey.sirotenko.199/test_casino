<?php 
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\data\Prize;
use app\models\data\PrizeObject;


class CasinoPage extends Model { 

    const ROTATION_PRICE = 100;
    const COEFFICENT = 15;

    public $user;
    public $prize;
    public $win_history; 
    public $errors = [];
    public $last_transaction_id = null;

    function __construct() {
        $this->user = User::findOne(['id' => Yii::$app->user->id]);
        $this->win_history = WinHistory::find()->all();
    }
    
    public function renderPrize() {

        if(WinHistory::getUserBonuses() < self::ROTATION_PRICE) {
            $this->errors[] = 'You do not have enough bonuses to perform this operation';
            return;
        }

        WinHistory::getBonusesFromTransaction(self::ROTATION_PRICE);
        $rand = rand(0,100);
        if($rand < 20) {
            $cash_val = rand(10, 20);
            $prize = new Prize('cash', $cash_val);
        }elseif($rand >= 20 && $rand < 40) {
            $bonus_val = rand(100, 500);
            $prize = new Prize('bonus',$bonus_val);
        }elseif($rand >= 40 && $rand <= 100) {
            $prize = new PrizeObject('object');
        }
        
        $this->user->cash_val = WinHistory::getUserCash();
        $this->user->bonus_val = WinHistory::getUserBonuses();

        $this->last_transaction_id = $this->createWinHistoryLog([
            'user_id' => Yii::$app->user->id,
            'type' => $prize->type,
            'value' => $prize->win_val,
            'promo' => isset($prize->img_url) ? $prize->img_url : null,
            'created_at' => strtotime("now"),
        ]); 

        $this->user->save();
        $this->prize = $prize;
    }

    private function createWinHistoryLog($data) {
        $winHistoryLog = new WinHistory();

        $winHistoryLog->user_id     = $data['user_id'];
        $winHistoryLog->type        = $data['type'];
        $winHistoryLog->value       = $data['value'];
        $winHistoryLog->promo       = $data['promo'];
        $winHistoryLog->created_at  = $data['created_at'];

        $winHistoryLog->save();
        return $winHistoryLog->getPrimaryKey();
    }
}