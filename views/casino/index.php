<div class="action-wrapper">
    <div class="row">
        <div class="col-md-12">
            <h1>Simple roulette</h1>
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if(isset($user)) { ?>
                <p class="score-title">You score: <b><?= $user->cash_val?></b> cash, <b><?= $user->bonus_val?></b> bonuses</p>
                <p>price per one click <b>100</b> bonuses</p>
                <a href="/index.php?r=casino/begin" class="btn btn-primary start-action">Click on me!</a>
                <a href="/index.php?r=user/logout" class="btn btn-primary start-action">Exit</a>
            <?php } ?>
            <?php if(isset($errors) && count($errors) != 0) { ?>
                <div style="margin-top: 10px;">
                    <?php foreach($errors as $error) { ?>
                        <p class="text-danger"><?= $error ?></p>
                    <?php } ?>                                
                </div>
            <?php } ?>
            <?php if(isset($prize)) {?>
                <div class="price-wrapper">
                    <h2><?=$prize->win_title?></h2>
                    <?php if($prize->type == 'cash') { ?>
                        <a href="/index.php?r=casino/convert&t_id=<?= $last_transaction_id?>" class="btn btn-primary">Convert on bonus</a>
                        <a href="/index.php?r=casino/cash-out&t_id=<?= $last_transaction_id?>" class="btn btn-primary" style="margin-left: 10px">Cash out</a>
                    <?php } elseif($prize->type == 'object') { ?>
                        <div>
                            <div class="prize-img-wrapper">
                                <img src="<?=$prize->img_url?>" alt="you prize!" />
                            </div>
                        </div>
                        <a href="/index.php?r=casino/refuse&t_id=<?= $last_transaction_id?>" class="btn btn-primary" data-action="refuse">Refuse</a>
                        <a href="/index.php?r=casino/send&t_id=<?= $last_transaction_id?>" class="btn btn-primary" data-action="send" style="margin-left: 10px">Send on my e-mail</a>
                    <?php } ?>
                </div>
            <?php } ?>
            <hr />
            <?php if(isset($win_history)) { ?>
            <div class="user-prize_list">
                <div class="user-prize_title">
                    <h2>You win history:</h2>
                    <div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Reward type</td>
                                    <td>Value</td>
                                    <td>Date of receiving</td>
                                    <td>Control</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($win_history as $h_key => $h_val) { ?>
                                    <tr>
                                        <td><?= $h_key + 1?></td>
                                        <td><?= $h_val['type']?></td>
                                        <td><?= $h_val['value'] != 0 ? $h_val['value'] : 'no' ?></td>
                                        <td><?= date('Y-m-d H:m:s', $h_val['created_at']) ?></td>
                                        <td>
                                            <?php if($h_val['type'] == 'cash') { ?>
                                            <a href="/index.php?r=casino/convert&t_id=<?= $h_val['id'] ?>">Convert to bonuses</a>
                                            <span> / </span>
                                            <a href="/index.php?r=casino/cash-out&t_id=<?= $h_val['id'] ?>">Cash out</a>
                                            <?php } elseif($h_val['type'] == 'object') { ?>
                                            <a href="/index.php?r=casino/refuse&t_id=<?= $h_val['id'] ?>">Refuse</a>
                                            <span> / </span>
                                            <a href="/index.php?r=casino/send&t_id=<?= $h_val['id'] ?>">Send on e-mail</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
