<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%win_history}}`.
 */
class m200508_145923_create_win_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%win_history}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'value' => $this->integer()->notNull(),
            'promo' => $this->string(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%win_history}}');
    }
}
